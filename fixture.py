import string
import random

import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from login_page_pop import LoadPage
from project_page_pop import ProjectPage


administrator_email = 'administrator@testarena.pl'


def get_random_string(length):
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=length))


random_project_name = get_random_string(10)
random_project_prefix = get_random_string(5)

@pytest.fixture()
def browser():
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1920, 720)
    load_page = LoadPage(browser)
    load_page.get_to_test_arena_website()
    load_page.login_to_test_arena(administrator_email,'sumXQQ72$L')

    yield browser
    browser.quit()

@pytest.fixture()
def project_page(browser):
    project_page = ProjectPage(browser)
    project_page.get_to_project_page()

    yield browser

@pytest.fixture()
def add_project(project_page):

    project_page.find_elements(By.CSS_SELECTOR, 'a.button_link')[0].click()
    project_page.find_element(By.CSS_SELECTOR, '#name').send_keys(random_project_name)
    project_page.find_element(By.CSS_SELECTOR, '#prefix').send_keys(random_project_prefix)
    project_page.find_element(By.CSS_SELECTOR, '#description').send_keys('just a sample')
    project_page.find_element(By.CSS_SELECTOR, '#save').click()

    yield project_page