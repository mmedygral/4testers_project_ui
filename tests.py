from selenium.webdriver.common.by import By
from fixture import browser, project_page, random_project_name, add_project


def test_if_can_log_in(browser):
    assert browser.find_element(By.CSS_SELECTOR, '.user-info small').text == 'administrator@testarena.pl'


def test_if_administration_panel_can_be_openned(project_page):
    assert project_page.find_element(By.CSS_SELECTOR, '.content_title').text == 'Projekty'

def test_if_project_has_been_added(add_project):
    assert add_project.find_element(By.CSS_SELECTOR, '.content_label_title').text == random_project_name

def test_if_number_of_added_project_is_correct(add_project):
    add_project.find_element(By.CSS_SELECTOR, '.activeMenu').click()
    assert add_project.find_elements(By.CSS_SELECTOR, 'table tr:first-child td')[0].text == random_project_name
