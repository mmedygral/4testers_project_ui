from selenium.webdriver.common.by import By

administrator_email = 'administrator@testarena.pl'

class LoadPage:
    def __init__(self, browser):
        self.browser = browser

    def get_to_test_arena_website(self):
        self.browser.get('http://demo.testarena.pl/zaloguj')

    def login_to_test_arena(self, email, password):
        self.browser.find_element(By.CSS_SELECTOR, '#email').send_keys(email)
        self.browser.find_element(By.CSS_SELECTOR, '#password').send_keys(password)
        self.browser.find_element(By.CSS_SELECTOR, '#login').click()


    def open_administration_panel(self):
        self.browser.find_element(By.CSS_SELECTOR, '.icon_tools.icon-20').click()