from selenium.webdriver.common.by import By

class ProjectPage:

    def __init__(self, browser):
        self.browser = browser

    def get_to_project_page(self):
        self.browser.find_element(By.CSS_SELECTOR, '.icon_tools.icon-20').click()
